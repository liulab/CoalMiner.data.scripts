Title: Coal-Miner: mapping the genomic architecture of
quantitative traits with complex evolutionary
origins

Authors: Hussein A. Hejase & Natalie VandePol & Gregory A. Bonito & Patrick P. Edger & Kevin J. Liu

LICENSE: All data and scripts are distributed under the terms of the GNU General Public License as published by the Free Software Foundation. You can distribute or modify it under the terms of the GNU General Public License either version 3 of the License or any later version.

To run Coal-Miner, you need to install the following:

EIGENSTART: http://genetics.med.harvard.edu/reich/Reich_Lab/Software.html

GEMMA: http://www.xzlab.org/software.html

LAPACK package: http://www.netlib.org/lapack/

The GNU Scientific Library (GSL): http://www.gnu.org/software/gsl/

The latest R version (v3.2.0)


a. Enclosed in the simulation-study folder you can find all the simulated genotypic and phenotypic data:

>genetic-drift-with-no-gene-flow is a folder containing data for the hybridization frequency gamma of 0 (ILS with no gene flow model condition)

>genetic-drift-with-gene-flow is a folder containing data for the hybridization frequency gamma of 0.5 (ILS with gene flow model condition)

>gene-flow-with-positive-selection is a folder containing simulations that include gene flow with positive selection

>genetic-drift-with-positive-selection is a folder containing simulations that include genetic drift with positive selection

>isolation-with-migration

>recombination is a folder containing simulations that include recombination

>vary-admixture-time

>vary-split-time

>single-causal-locus contains genotypic and phenotypic data for the single-causal-locus model condition

>two-causal-loci contains genotypic and phenotypic data for the two-causal-loci model condition

>three-causal-loci contains genotypic and phenotypic data for the three-causal-loci model condition

>causal_loci_&lt;replicate&gt;.txt contains the position of the causal sites (20 causal sites per replicate)

>phenotype_&lt;replicate&gt;.txt contains the quantitative trait for each individual

>int&lt;replicate&gt;.txt contains the breakpoints

>exp&lt;replicate&gt;.txt contains the genotypic data

b. The following are scripts (located in the simulation-study/scripts folder) used to run the experiments:

1. run_causal.perl, parse_causal.sh, coalminer.sh: scripts used to run stage three of Coal-Miner

2. gemma.sh: a script used to run GEMMA

3. run_par.perl, parse_pc.sh: scripts used in stage two of Coal-Miner to get the local principal components

4. runp_gemma.sh, parse_MLE_null_model.sh, find_max_partition.R: scripts used to infer candidate loci in stage two of Coal-Miner

5. parse_geno.R, parse_EIGENSTRAT.R, and gemma_process.py: scripts used to format the data to be used by EIGENSTRAT, GEMMA, Coal-Map, and Coal-Miner

6. parse_pc_coal-map.sh, run_gemma_coal-map.sh, run_par_coal-map.perl, runp_gemma_coal-map.sh: scripts used to run Coal-Map

7. run_FGT.sh: script used to run FGT in stage one of Coal-Miner

The empirical data for the bacteria dataset belonging to the *Burkholderiaceae* can be downloaded from https://www.patricbrc.org

The empirical data for the *Arabidopsis* dataset can be downloaded from http://1001genomes.org


The empirical data for the *Heliconius* can be downloaded by referring to the following paper:

Supple, Megan A., *et al.* "Genomic architecture of adaptive color pattern divergence and convergence in Heliconius butterflies." Genome research (2013): gr-150615.
