numRep=20
for k in `seq 1 $numRep`;
do
	gemma -g geno_$k.txt -p phenotype_$k.txt -maf 0 -a annot_$k.txt -gk -o kinship$k
	gemma -g geno_$k.txt -p phenotype_$k.txt -n 1 -c EIGS_con/example$k.pca -maf 0 -r2 1 -lmax 0.00001 -a annot_$k.txt -k output/kinship$k.cXX.txt -lmm 2 -o stat_lmm$k  
	awk '{print $9}' output/stat_lmm$k.assoc.txt > output/pval$k.txt
        awk '{print $8}' output/stat_lmm$k.assoc.txt > output/lscore$k.txt
done
rm output/stat_lmm*
rm output/kinship*
mv output/ con
cp causal_loci* con
