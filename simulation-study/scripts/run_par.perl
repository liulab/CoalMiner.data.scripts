#!/usr/bin/perl

$ENV{'PATH'} = "/mnt/home/hijazihu/EIG6.0.1/bin:$ENV{'PATH'}"; 
# MUST put smartpca bin directory in path for smartpca.perl to work

my $numRep=20;
my $numGeneTrees=10;
for (my $k=1; $k <= $numRep; $k++) {
	for (my $g=1; $g <= $numGeneTrees; $g++) {
		#get local fixed effect
		$command = "smartpca.perl";
		$command .= " -i EIGS_input/geno_" . "$k" . "_$g.txt ";
		$command .= " -a EIGS_input/snp_" . "$k" . "_$g.txt ";
		$command .= " -b EIGS_input/ind$k.txt ";
		$command .= " -k 5 ";
		$command .= " -q YES ";
		$command .= " -o EIGS_par/example_" . "$k" . "_$g.txt ";
		$command .= " -p EIGS_par/example_" . "$k" . "_$g.txt ";
		$command .= " -e EIGS_par/example_" . "$k" . "_$g.txt ";
		$command .= " -l EIGS_par/example_" . "$k" . "_$g.txt ";
		$command .= " -m 5 ";
		$command .= " -t 2 ";
		$command .= " -s 6.0 ";
		print("$command\n");
		system("$command");
	}
}
