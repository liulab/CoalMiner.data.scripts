###script used to simulate the trait###
parse_geno.R

###Stage 1###
qsub -v ARG1=<replicate> run_FGT.sh

###scripts used to partition the genomes###
parse_EIGENSTRAT.R
gemma_process.py

###Run GEMMA###
sh gemma.sh

###Run EIGENSTRAT###
perl run_con.perl

###Get top five principal components for each locus to be used in stage 2###
perl run_par.perl
sh parse_pc.sh

###Stage 2###
sh runp_gemma.sh <proportion of candidate loci>
sh parse_MLE_null_model.sh
Rscript find_max_partition.R <number of candidate loci>

###Get top principal component for each locus to be used in stage 3###
perl run_causal.perl
sh parse_causal.sh

###Stage 3###
sh coalminer.sh <folder name> <proportion of candidate loci> 

