#!/bin/bash
numRep=20
numGeneTrees=10
for k in `seq 1 $numRep`;
do
	gemma -g geno_$k.txt -p phenotype_$k.txt -maf 0 -a annot_$k.txt -gk -o kinship$k
	for j in `seq 1 $numGeneTrees`;
	do
		echo $k
 		gemma -g genoK_$k'_'$j.txt -p phenotype_$k.txt -n 1 -c EIGS_par/coalmap_example_combined_nonlocal_$k'_'$j.txt -maf 0 -r2 1 -lmax 0.00001 -a annotK_$k'_'$j.txt -k output/kinship$k.cXX.txt -lmm 2 -o stat_lmm$k'_'$j		
		awk '{print $9}' output/stat_lmm$k'_'$j.assoc.txt > output/pval$k'_'$j.txt
        	awk '{print $8}' output/stat_lmm$k'_'$j.assoc.txt > output/lscore$k'_'$j.txt
	done
done
rm output/stat_lmm*
rm output/kinship*
mv output/ par
cp causal* par
