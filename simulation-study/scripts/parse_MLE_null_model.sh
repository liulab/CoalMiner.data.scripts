for i in `seq 1 20`;
do
	for j in `seq 1 10`;
	do
		grep '## MLE log-likelihood in the null model = ' local-only/stat_lmm$i'_'$j.log.txt | cut -d' ' -f9 >> marker$i.txt
	done
done
