#!/bin/bash
numRep=20
numGeneTrees=10
for k in `seq 1 $numRep`;
do
	for j in `seq 1 $numGeneTrees`;
	do
		sed -i -e 1,2d EIGS_causal/example_$k'_'$j.txt
	done
done
