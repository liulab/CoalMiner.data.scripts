#!/bin/bash
numRep=20
for k in `seq 1 $numRep`;
do
	numGeneTrees=$(cat int$k.txt | wc -l)
        /mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g geno_$k.txt -p phenotype_$k.txt -a annot_$k.txt -gk -o kinship$k
        for j in `seq 1 $numGeneTrees`;
        do
                echo $k
                /mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g genoK_$k'_'$j.txt -p phenotype_$k.txt -n 1 -c EIGS_par/example_$k'_'$j.txt -scaling $1 -maf 0 -r2 1 -a annotK_$k'_'$j.txt -k output/kinship$k.cXX.txt -lmax 1 -lmm 2 -o stat_lmm$k'_'$j
        done
done
mv output/ local-only
cp causal* local-only
