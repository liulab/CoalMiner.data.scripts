from sys import argv
import csv
numGeneTrees = 10
numSegSites = 250
numSamples = 1000
numReplicates = 20

for replicate in range(1,numReplicates+1):
	filename = "int" + str(replicate) + ".txt"
	fin=open(filename,'r')
	Llist = fin.readline()
	intervals = Llist.split(" ")

	filename_annot = "annot_" + str(replicate) + ".txt"
	annot = open(filename_annot, 'w')

	chr = 1
	genomeSize = numGeneTrees * numSegSites
	
	genetree = 1
	filename_annotK = "annotK_" + str(replicate) + "_" + str(genetree) + ".txt"
	annotK = open(filename_annotK, 'w')
	j = 1

	for i in range(1,genomeSize+1):
		snpname = "rs_" + str(i)
		pos = i
		annot.write(snpname)
		annot.write("\t")
		annot.write(str(pos))
		annot.write("\t")
		annot.write(str(chr))
		annot.write("\n")

        	snpname = "rs_" + str(j)
        	pos = j	
		annotK.write(snpname)
        	annotK.write("\t")
        	annotK.write(str(pos))
        	annotK.write("\t")
        	annotK.write(str(chr))
        	annotK.write("\n")
		j = j + 1

		if j==(int(intervals[2]) - int(intervals[0]) +2):
			j = 1
			genetree = genetree + 1
			filename_annotK = "annotK_" + str(replicate) + "_" + str(genetree) + ".txt"
			annotK = open(filename_annotK, 'w')
        		Llist = fin.readline()
        		intervals = Llist.split(" ")


        filename = "int" + str(replicate) + ".txt"
        fin=open(filename,'r')

	s = 1
	filename_geno = "geno_" + str(replicate) + ".txt"
        geno = open(filename_geno, 'w')
	for g in range(1,numGeneTrees+1):
        	print g
		filename_geno = "exp" + str(replicate) + ".txt"
                temp=open(filename_geno)
                D = csv.reader(temp, delimiter='\t')
                table = [row for row in D]

		filename_genoK = "genoK_" + str(replicate) + "_" + str(g) + ".txt"
        	genoK = open(filename_genoK, 'w')

        	Llist = fin.readline()
	        intervals = Llist.split(" ")
		
		for i in range(int(intervals[0])-1, int(intervals[2])):
			snpname = "rs_" + str(s)
			s = s + 1
			geno.write(snpname)
                	geno.write(", ")
			
			snpname = "rs_" + str(i+1)
              		genoK.write(snpname)
                        genoK.write(", ")

			sumallelefreq = 0
			for j in range(0,numSamples):
				sumallelefreq = sumallelefreq + int(table[j][i])
			if sumallelefreq<numSamples/2:
				minor_allele = 1	
				major_allele = 0
			elif sumallelefreq>=numSamples/2:
				minor_allele = 0
                                major_allele = 1
			geno.write(str(minor_allele))
                        geno.write(", ")
                        geno.write(str(major_allele))
			
			genoK.write(str(minor_allele))
                        genoK.write(", ")
                        genoK.write(str(major_allele))

                        for j in range(0,numSamples):
                        	geno.write(", ")
                                geno.write(table[j][i])
                                genoK.write(", ")
                                genoK.write(table[j][i])
			geno.write("\n")
			genoK.write("\n")

