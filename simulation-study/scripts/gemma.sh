#!/bin/bash
numRep=20
for k in `seq 1 $numRep`;
do
  gemma -g geno_$k.txt -p phenotype_$k.txt -a annot_$k.txt -gk 1 -o kinship$k
  gemma -g geno_$k.txt -p phenotype_$k.txt -n 1 -maf 0 -r2 1 -a annot_$k.txt -k output/kinship$k.cXX.txt -lmm 2 -o stat_lmm$k
  awk '{print $9}' output/stat_lmm$k.assoc.txt > output/pval$k.txt
done
mv output/ gemma-results
cp causal* gemma-results
