#!/bin/bash
numRep=20
numGeneTrees=10
for k in `seq 1 $numRep`;
do
        sed -i -e 1,6d EIGS_con/example$k.pca
	sed -i -e 1,6d EIGS_par/coalmap_example$k.pca

	for j in `seq 1 $numGeneTrees`;
	do
		sed -i -e 1,6d EIGS_par/coalmap_example_$k'_'$j.txt
		sed -i -e 1,6d EIGS_par/coalmap_exampleNonlocal_$k'_'$j.txt
		paste EIGS_par/coalmap_example_$k'_'$j.txt EIGS_par/coalmap_exampleNonlocal_$k'_'$j.txt > EIGS_par/coalmap_example_combined_nonlocal_$k'_'$j.txt
		paste EIGS_par/coalmap_example_$k'_'$j.txt EIGS_par/coalmap_example$k.pca > EIGS_par/coalmap_example_combined_global_$k'_'$j.txt
	done
done
