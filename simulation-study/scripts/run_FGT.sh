#!/bin/bash
#Example PBS script to run R
#PBS -l nodes=1:ppn=1,walltime=04:00:00,mem=4gb
#PBS -N exp

#change direc
cd ${PBS_O_WORKDIR}

module load NumPy

i=$ARG1
echo $i
exp=exp$i.txt
res=res$i.txt
echo $exp
echo $res
python genotypeScan_sample_usage.py $exp > $res

