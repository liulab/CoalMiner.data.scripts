#!/bin/bash
curr=$1
numRep=20
numGeneTrees=10
for k in `seq 1 $numRep`;
do
        c=$(head -n $k markers.txt | tail -n 1)
        IFS=',' read -r -a array <<< "$c"
        first=${array[0]}
        second=${array[1]}
	third=${array[2]}
        /mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g geno_$k.txt -p phenotype_$k.txt -a annot_$k.txt -gk 1 -o kinship$k
        for j in `seq 1 $numGeneTrees`;
        do
                if [[ $j == $first ]]
                then
                        /mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g genoK_$k'_'$j.txt -p phenotype_$k.txt -n 1 -c EIGS_causal/example_$k'_'$j.txt -scaling $2 -maf 0 -r2 1 -a annotK_$k'_'$j.txt -k output/kinship$k.cXX.txt -lmax 1 -lmm 2 -o stat_lmm$k'_'$j
		elif [[ $j == $second ]]
                then
               		/mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g genoK_$k'_'$j.txt -p phenotype_$k.txt -n 1 -c EIGS_causal/example_$k'_'$j.txt -scaling $2 -maf 0 -r2 1 -a annotK_$k'_'$j.txt -k output/kinship$k.cXX.txt -lmax 1 -lmm 2 -o stat_lmm$k'_'$j
                elif [[ $j == $third ]]
                then
                        /mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g genoK_$k'_'$j.txt -p phenotype_$k.txt -n 1 -c EIGS_causal/example_$k'_'$j.txt -scaling $2 -maf 0 -r2 1 -a annotK_$k'_'$j.txt -k output/kinship$k.cXX.txt -lmax 1 -lmm 2 -o stat_lmm$k'_'$j
		else
			/mnt/home/hijazihu/gemma/gemma-0.94/bin/./gemma -g genoK_$k'_'$j.txt -p phenotype_$k.txt -n 1 -maf 0 -r2 1 -a annotK_$k'_'$j.txt -k output/kinship$k.cXX.txt -lmm 2 -o stat_lmm$k'_'$j
		fi
                awk '{print $9}' output/stat_lmm$k'_'$j.assoc.txt > output/pval$k'_'$j.txt
        done
done
rm output/stat_lmm*
rm output/kinship*
mv output/ $curr
cp causal* $curr
